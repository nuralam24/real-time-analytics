import rateLimit from "express-rate-limit";

const serverRequest = rateLimit({
    windowMs: 2 * 60 * 1000, // 2 minutes
    max: 500, // limit each IP to 500 requests per windowMs,
    message: "Too many request attempts. Please try again after 5 minutes!",
    statusCode: 429,
    headers: true
});

export { serverRequest };
