import { Request, Response } from 'express';
import quizResultModel from '../models/quizResult';
import { getIo } from '../utils/socket';

const addQuizResult = async (req: Request, res: Response) => {
    const { userId, score, totalQuestions } = req.body;
    try {
        const quizResult = new quizResultModel({ userId, score, totalQuestions });
        await quizResult.save();
        const io = getIo();
        io.emit('newQuizResult', quizResult);

        return res.status(201).json({
            data: quizResult,
            success: true,
            message: 'Added quiz result successfully!',
        });
    } catch (error) {
        return res.status(error.status || 500).json({
            data: null,
            success: false,
            message: error.message || 'Internal Server Error Occurred!'
        });
    }
};

const getQuizResults = async (req: Request, res: Response) => {
    const { start, end, resultsPerPage, page } = req.query;

    const resultsPerPageNumber = Number(resultsPerPage) || 20;
    const pageNumber = Number(page) >= 1 ? Number(page) : 1;

    try {
        const matchStage = { 
            timestamp: { 
                $gte: new Date(start as string), 
                $lte: new Date(end as string) 
            } 
        };

        // Get the total count of documents
        const totalCount = await quizResultModel.countDocuments(matchStage);

        // Get the paginated results
        const results = await quizResultModel.find(matchStage)
            .skip((pageNumber - 1) * resultsPerPageNumber)
            .limit(resultsPerPageNumber);

        return res.status(200).json({
            data: {
                totalDataLength: totalCount,
                page: pageNumber,
                resultsPerPage: resultsPerPageNumber,
                data: results
            },
            success: true,
            message: "View successfully!"
        });
    } catch (error) {
        return res.status(error.status || 500).json({
            data: null,
            success: false,
            message: error.message || 'Internal Server Error Occurred!'
        });
    }
};

const getAggregates = async (req: Request, res: Response) => {
    const { start, end, interval, resultsPerPage, page } = req.query;

    const resultsPerPageNumber = Number(resultsPerPage) || 20;
    const pageNumber = Number(page) >= 1 ? Number(page) : 1;

    try {
        const matchStage = { 
            timestamp: { 
                $gte: new Date(start as string),
                $lte: new Date(end as string)
            }
        };

        let groupStage;
        switch (interval) {
            case 'hourly':
                groupStage = {
                    $group: {
                        _id: {
                            year: { $year: "$timestamp" },
                            month: { $month: "$timestamp" },
                            day: { $dayOfMonth: "$timestamp" },
                            hour: { $hour: "$timestamp" }
                        },
                        averageScore: { $avg: '$score' },
                        totalScore: { $sum: '$score' },
                        totalAttempts: { $sum: 1 }
                    }
                };
                break;
            case 'daily':
                groupStage = {
                    $group: {
                        _id: {
                            year: { $year: "$timestamp" },
                            month: { $month: "$timestamp" },
                            day: { $dayOfMonth: "$timestamp" }
                        },
                        averageScore: { $avg: '$score' },
                        totalScore: { $sum: '$score' },
                        totalAttempts: { $sum: 1 }
                    }
                };
                break;
            default:
                return res.status(400).json({ error: 'Invalid interval specified' });
        }

        // Get the total count of aggregates
        const totalCountAggregate = await quizResultModel.aggregate([
            { $match: matchStage },
            groupStage,
            { $count: 'totalCount' }
        ]);

        const totalCount = totalCountAggregate.length > 0 ? totalCountAggregate[0].totalCount : 0;

        // Get the paginated aggregates
        const aggregates = await quizResultModel.aggregate([
            { $match: matchStage },
            groupStage,
            { $skip: (pageNumber - 1) * resultsPerPageNumber },
            { $limit: resultsPerPageNumber }
        ]);

        return res.status(200).json({
            data: {
                totalDataLength: totalCount,
                page: pageNumber,
                resultsPerPage: resultsPerPageNumber,
                data: aggregates
            },
            success: true,
            message: "View successfully!"
        });
    } catch (error) {
        return res.status(error.status || 500).json({
            data: null,
            success: false,
            message: error.message || 'Internal Server Error Occurred!'
        });
    }
};


export { addQuizResult, getQuizResults, getAggregates };
