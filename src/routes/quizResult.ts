import { Router } from 'express';
import verifyToken from '../middlewares/verifyToken';
import permission from '../middlewares/permission';
import { addQuizResult, getQuizResults, getAggregates } from '../controllers/quizResultController.';
import {
    addQuizResultValidator, getQuizResultsValidator, getAggregatesValidator
} from "../validations/quizResultValidation";

const router = Router();

router.post('/', addQuizResultValidator, [verifyToken, permission('user', 'admin')], addQuizResult);
router.get('/', getQuizResultsValidator, [verifyToken, permission('admin')], getQuizResults);
router.get('/aggregates', getAggregatesValidator, [verifyToken, permission('admin')], getAggregates);


export default router;