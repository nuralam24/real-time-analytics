import userRouter from './user';
import quizResultRouter from './quizResult';

const routes = [
    { path: '/user', controller: userRouter },
    { path: '/quiz-results', controller: quizResultRouter },
];

const setupRoutes = (app: any) => {
    for (const route of routes) {
        app.use(`/api/v1${route.path}`, route.controller);
    }
};

export default setupRoutes;
