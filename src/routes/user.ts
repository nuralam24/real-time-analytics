import { Router } from 'express';
import verifyToken from '../middlewares/verifyToken';
import permission from '../middlewares/permission';
import {registration, login} from '../controllers/userController';
import { registrationValidator, loginValidator } from "../validations/userValidation";

const router = Router();

router.post('/registration', [registrationValidator], registration);
router.post('/login', [loginValidator], login);

export default router;