import { Request, Response } from 'express';


const internalServerError = (res: Response, logMsg: string, error: string) => {
    console.log(logMsg);
    console.log('Error = ', error);
    return res.status(500).json({
        data: null,
        success: false,
        message: "Internal Server Error Occurred!"
    });
};

const noDataFoundSuccess = (res: Response) => {
    return res.status(400).json({
        data: [],
        success: true,
        message: "No data found!"
    });
};

const noDataFoundSuccessWithPagination = (res: Response, resultsPerPage: number) => {
    return res.status(200).json({
        data: {
            resultsPerPage,
            totalDataLength: 0,
            data: []
        },
        success: true,
        message: "No data found!"
    });
};

const noDataFoundFailed = (res: Response) => {
    return res.status(404).json({
        data: null,
        success: false,
        message: "No data found!"
    });
};

const alreadyExists = (res: Response, msg: string) => {
    return res.status(409).json({
        data: null,
        success: false,
        message: `${msg} already exists!`
    });
};

const invalidRequest = (res: Response, msg: string) => {
    return res.status(400).json({
        data: null,
        success: false,
        message: `Request invalid. ${msg}`
    });
};

const createSuccess = (res: Response, data: any, msg: string) => {
    return res.status(201).json({
        data: data || '',
        success: true,
        message: `${msg}`
    });
};

const updateSuccess = (res: Response, data: any, msg: string) => {
    return res.status(202).json({
        data: data || '',
        success: true,
        message: `${msg}`
    });
};

const dataViewSuccess = (res: Response, data: any) => {
    return res.status(200).json({
        data: data || '',
        success: true,
        message: 'View successfully!'
    });
};

const dataViewSuccessWithPagination = (res: Response, resultsPerPage: number, totalDataLength: number, data: any) => {
    return res.status(200).json({
        data: {
            resultsPerPage,
            totalDataLength,
            data
        },
        success: true,
        message: "View successfully!"
    });
};

const deleteSuccess = (res: Response) => {
    return res.status(202).json({
        success: true,
        message: 'Deleted successfully!'
    });
};

const incorrectInputData = (res: Response, msg: string) => {
    return res.status(401).json({
        data: null,
        success: false,
        message: `Request invalid. ${msg}`
    });
};

const loggedInSuccess = (res: Response, data: any) => {
    return res.status(200).json({
        data,
        success: true,
        message: 'Logged in Successfully!',
    });
};

export {
    internalServerError,
    noDataFoundSuccess,
    noDataFoundSuccessWithPagination,
    noDataFoundFailed,
    alreadyExists,
    createSuccess,
    updateSuccess,
    dataViewSuccess,
    dataViewSuccessWithPagination,
    invalidRequest,
    deleteSuccess,
    incorrectInputData,
    loggedInSuccess
};
