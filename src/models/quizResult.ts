import { Schema, Document, model } from 'mongoose';

export interface IQuizResult extends Document {
    timestamp: Date;
    userId: string;
    score: number;
    totalQuestions: number;
}

const QuizResultSchema: Schema = new Schema({
    timestamp: { type: Date, default: Date.now },
    userId: { type: String, required: true },
    score: { type: Number, required: true },
    totalQuestions: { type: Number, required: true },
});

const QuizResult = model<IQuizResult>('QuizResult', QuizResultSchema);
export default QuizResult;

