import request from 'supertest';
import app from '../app';
import mongoose, { ConnectOptions } from 'mongoose';
import dotenv from 'dotenv';
dotenv.config();
import chalk from 'chalk';
import { exit } from 'process';

describe('Quiz Results API', () => {
    beforeAll(async () => {
        const DB_URL: string | undefined = process.env.DB_URL;
        if (!DB_URL) {
            console.error(chalk.red.bold('DB_URL is not defined in the environment variables!'));
            exit(1);
        }
        const options: ConnectOptions = {
            connectTimeoutMS: 30000,
            tlsInsecure: true
        };
        await mongoose.connect(DB_URL, options);
    });

    afterAll(async () => {
        await mongoose.connection.close();
    });

    it('should add a quiz result', async () => {
        const res = await request(app)
            .post('/api/quiz-results')
            .send({
                userId: 'testuser',
                score: 80,
                totalQuestions: 100
            });
        expect(res.statusCode).toEqual(201);
        expect(res.body).toHaveProperty('score', 80);
    });

    it('should retrieve quiz results within a time range', async () => {
        const start = new Date('2023-01-01').toISOString();
        const end = new Date('2023-12-31').toISOString();

        const res = await request(app)
            .get('/api/quiz-results')
            .query({ start, end });

        expect(res.statusCode).toEqual(200);
        expect(res.body).toBeInstanceOf(Array);
    });

    it('should retrieve aggregates for a given time range and interval', async () => {
        const start = new Date('2024-06-07').toISOString();
        const end = new Date('2023-06-07').toISOString();

        const res = await request(app)
            .get('/api/quiz-results/aggregates')
            .query({ start, end, interval: 'daily' });

        expect(res.statusCode).toEqual(200);
        expect(res.body).toBeInstanceOf(Array);
        expect(res.body[0]).toHaveProperty('averageScore');
        expect(res.body[0]).toHaveProperty('totalScore');
        expect(res.body[0]).toHaveProperty('totalAttempts');
    });
});
