import express from 'express';
import routes from './routes';
import middlewares from './middlewares';
import errorHandle from './middlewares/errorHandle';
import db from './config/db';

const app = express();
app.enable('trust proxy'); // only if behind a reverse proxy (Heroku, AWS ELB, Nginx, etc)

middlewares(app); // initialize middlewares
db(app); // database connection
routes(app); // initialize routes
errorHandle(app); // error handlers

export default app;

