import Joi from "joi";
import { validationHandler } from "../utils/validationHandler";

const addQuizResult = Joi.object({
    userId: Joi.string().hex().length(24).required(),
    score: Joi.number().required(),
    totalQuestions: Joi.number().required(),
});

const getQuizResults = Joi.object({
    start: Joi.date().required(),
    end: Joi.date().required(),
    resultsPerPage: Joi.number().required(),
    page: Joi.number().required(),
});

const getAggregates = Joi.object({
    start: Joi.date().required(),
    end: Joi.date().required(),
    interval: Joi.string().required(),
    resultsPerPage: Joi.number().required(),
    page: Joi.number().required(),
});

const addQuizResultValidator = validationHandler({
  body: addQuizResult,
});

const getQuizResultsValidator = validationHandler({
    query: getQuizResults,
});

const getAggregatesValidator = validationHandler({
  query: getAggregates,
});

export {
    addQuizResultValidator,
    getQuizResultsValidator,
    getAggregatesValidator
};