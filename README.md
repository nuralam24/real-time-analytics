# Real Time Analytics

## Description
A RESTful API for managing real_time_analytics, built with Node.js, TypeScript, Express, MongoDB & WebSocket. Includes JWT authentication & Authorization.

## Features
- Create, View real_time_analytics
- Real Time result post
- JWT-based authentication
- Role-based access control

## Setup

### Prerequisites
- Docker
- Docker Compose

### Docker Build
- docker-compose up --build

### Manual Installation

1. Clone the repository
   ```bash
   git clone https://gitlab.com/nuralam24/real-time-analytics
   cd real_time_analytics
   .env file must be added to the root directory. requested to follow .env.example file
   npm install
   npm run dev


### API ENDPOINT
# Auth
- POST: `http://localhost:4411/api/v1/user/registration`
- POST: `http://localhost:4411/api/v1/user/login`

# Quiz Results
- GET: `http://localhost:4411/api/v1/quiz-results` (Requires Authentication & Authorization)
- GET: `http://localhost:4411/api/v1/quiz-results/aggregates` (Requires Authentication & Authorization)
- POST: `http://localhost:4411/api/v1/quiz-results` (Requires Authentication & Authorization)

